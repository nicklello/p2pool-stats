use Modern::Perl;
use JSON::XS;
use Data::Dumper;

use LWP::UserAgent;
my $lwp_useragent;

use DateTime;

$| = 1;

my %getpeers_seen;

my $json_filename = 'p2pool-nodes-available.json';
my $seed_node = '172.18.0.191:9332';

my $json_xs = JSON::XS->new();

my $audit_start = DateTime->now->epoch();

# Build a list of hosts to query
my %seen;
my @hosts_to_query;
my @newnodes = ( $seed_node );
while ( scalar(@newnodes) ) {
    my @nonunique_peers = _get_peernodes(@newnodes);
    @newnodes = _unique_hosts(\@nonunique_peers, \%seen);

    push @hosts_to_query, @newnodes;
    print scalar @newnodes, " New Nodes ; ", scalar @hosts_to_query, " Possible Nodes\n";
}
undef(%seen);

my %seenagain;
my @unique_nodes = _unique_hosts(\@hosts_to_query, \%seenagain);
undef(%seenagain);
print scalar @unique_nodes, " Unique Nodes\n";


my $index_no = 1;
my %seenworking;
my @output = grep { $_ } map {
    my $stats = _get_stats($_);

    if ( $stats && !$seenworking{$stats->{working_host_link}} ) {
        my $n_miners = 0;
        my $hash_rate_raw = 0;
        map {
            $n_miners++;
            $hash_rate_raw += $stats->{miner_hash_rates}->{$_};
        } keys $stats->{miner_hash_rates};

        my $hash_rate = $hash_rate_raw . ' H/s';
        if ($hash_rate_raw > 1000000000000) {
            $hash_rate = sprintf("%.2f TH/s", $hash_rate_raw / 1000 / 1000 / 1000 / 1000);
        } elsif ($hash_rate_raw > 1000000000) {
            $hash_rate = sprintf("%.2f GH/s", $hash_rate_raw / 1000 / 1000 / 1000);
        } elsif ($hash_rate_raw > 1000000) {
            $hash_rate = sprintf("%.2f MH/s", $hash_rate_raw / 1000 / 1000);
        } elsif ($hash_rate_raw > 1000) {
            $hash_rate = sprintf("%.2f KH/s", $hash_rate_raw / 1000);
        }

        my ($junk, $ipaddr, $port) = split(/:/, $stats->{working_host_link});
        $ipaddr =~ s/\/\///g;
        $seenworking{$stats->{working_host_link}} = 1;
        my $location_info = _lookup_location_by_ip($ipaddr);

        no warnings;
        my $warnings = join("\n", @{$stats->{warnings}});
        {
            index   =>  sprintf("#%03d", $index_no++),
            name    =>  $location_info->{hostname},
            ip      =>  $ipaddr,
            port    =>  $port,
            host    =>  'false',
            fee     =>  sprintf("%.1f", $stats->{fee}//0),
            donation    =>  sprintf("%.1f", $stats->{donation_proportion}//0),
            uptime  =>  sprintf("%.2f", $stats->{uptime}/86400),
            efficiency  =>  sprintf("%.2f", $stats->{efficiency}//0 * 100),
            perfect =>  sprintf("%.2f", $stats->{efficiency_if_miner_perfect}//0 * 100),
            miners  =>  $n_miners,
            hashrate    =>  $hash_rate,
            hashrate_raw => $hash_rate_raw,
            getwork =>  sprintf("%.2f", $stats->{getwork}//0),
            peers   =>  $stats->{peers}->{incoming}//0 . '/' . $stats->{peers}->{outgoing}//0,
            version =>  _version_cleanup($stats->{version}),
            country =>  $location_info->{country},
            region  =>  $location_info->{region},
            city    =>  $location_info->{city},
            latitude => $location_info->{latitude},
            longitude => $location_info->{longitude},
            warnings    => $warnings//'',
        }
    }
} @unique_nodes;

print scalar @output, " nodes active and output to json file\n";

my $data = {
    audit_start =>  $audit_start,
    audit_end   =>  DateTime->now()->epoch(),
    nodes       =>  [ @output ],
};

open(my $ofh, ">", $json_filename) or die "Cannot open $json_filename";
print "Opened $json_filename\n";
print $ofh JSON::XS::encode_json($data), "\n";
close($ofh);
exit;

sub _lookup_location_by_ip
{
    my $ipaddr = shift;
    my $data;
	my $response = useragent()->get("http://ipinfo.io/$ipaddr");
	if ( $response->is_success ) {
		eval {
			$data = decode_json($response->decoded_content);
		     };
		if ( $@ ) {
			die "Problem decoding response", $@, $response->decoded_content;
		}
        my ($lat,$long) = split(/,/, $data->{loc});
        $data->{latitude} = $lat;
        $data->{longitude} = $long;

       if ( lc($data->{hostname}) eq 'no hostname' ) {
            $data->{hostname} = $ipaddr;
       }
    }
    return $data;
}

sub useragent()
{
	if ( ! $lwp_useragent ) {
		$lwp_useragent = LWP::UserAgent->new;
		$lwp_useragent->timeout(2);
        $lwp_useragent->agent(ref(__PACKAGE__)); # content_type('application/json');
	}
	return $lwp_useragent;
}

sub _version_cleanup
{
    my $str = shift;

    $str =~ s/\"//g;
    return $str;
}

sub _get_stats
{
    my $full_host = shift;
    my ($host, $port) = split(/:/, $full_host);
    my $host_url = 'http://' . $full_host;
    my $stats;

    my $response;
    $response = useragent->get($host_url . '/local_stats');
	if ( !$response->is_success ) {
        if ( $port != 9332 ) {
            $host_url = "http://$host:9332";
            $response = useragent->get($host_url . '/local_stats');
        }
	    if ( !$response->is_success ) {
            print "$host_url is not accessible\n";
            return undef;
        }
    }
    print "Got $host_url/local_stats\n";
	eval {
		$stats = decode_json($response->decoded_content);
	     };
	if ( $@ ) {
		print "Problem decoding response: " . $@ . "\n";
        return undef;
	}

    $response = useragent->get($host_url . '/web/version');
	if ( !$response->is_success ) {
        print "ERROR: Get of $host_url/web/version failed!\n";
        return undef;
    }
    $stats->{version} = $response->decoded_content;

    $stats->{working_host_link} = $host_url;
    return $stats;
}

sub _get_peernodes
{
    return map { _get_peernodes_sub($_) } @_;
}

sub _get_peernodes_sub
{
    my $host = shift;
    my ($hn, $port) = split(/:/, $host);
    my $host_url = 'http://' . $host;

##printf "_get_peernodes_sub 010\n";
    my $response = useragent->get($host_url . '/peer_addresses');
##printf "_get_peernodes_sub 020\n";
	if ( !$response->is_success ) {
        if ( $port != 9332 ) {
            return _get_peernodes_sub($hn . ':9332');
        }
        return ();
    }
##printf "_get_peernodes_sub 030\n";

    my @peers = map {
        my ($host, $port) = split(/:/, $_);
        $host =~ s/"//;
        $port //= 9333;
        $port--;

        "$host:$port"
    } split(/ /, $response->decoded_content);
##printf "_get_peernodes_sub 040\n";

##    printf "There are %2d peers connected to %s\n", scalar @peers, $host;

# Remove duplicates
##    @peers = map { $getpeers_seen{$_} = 1; $_ } grep { !$getpeers_seen{$_} } @peers;
    @peers = _unique_hosts(\@peers, \%getpeers_seen);
    printf "There are %2d unique new peers connected to %s\n", scalar @peers, $host;

# TODO: Validate peers

    return @peers;
}

sub _unique_hosts
{
    my ($hosts_with_port, $hosts) = @_;

    my @uniq_hosts = map {
        my ($hn, $port) = split(/:/, $_);

        ${$hosts}{$hn} = 1;
        $_;
    } grep {
        my ($hn, $port) = split(/:/, $_);

        !${$hosts}{$hn};
    } @$hosts_with_port;

    return @uniq_hosts;
}

