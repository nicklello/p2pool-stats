Batch Directory
---------------

Scripts used to a) Poll for nodes and b) Generate static html

explore_nodes_available.pl      - Recursively searches for connected nodes starting at a seed node,
                                  continues searching until no new nodes are found.
                                  Stats are then extracted from each unique node and saved into JSON file.

nodes-listing.tmpl              - Perl HTML::Template format file for the 'active nodes' webpage

build_static_webpages.pl        - Perl script to build the static webpages from the JSON data files.


Generated files ignored by git
------------------------------
p2nodes.html                    - HTML Output for the 'active nodes' webpage
