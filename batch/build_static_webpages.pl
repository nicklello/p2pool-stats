use Modern::Perl;
use JSON::XS;
use HTML::Template;
use Data::Dumper;
$| = 1;

my $json_filename = 'p2pool-nodes-available.json';
my $tmpl_filename = 'nodes-listing.tmpl';
my $tmpl_filenam2 = 'nodes-heatmap.tmpl';
my $html_filename = 'p2nodes.html';
my $html_filenam2 = 'p2nodes-heatmap.html';
my $json_xs = JSON::XS->new();


open(my $ifh, "<", $json_filename) or die "Cannot open $json_filename";
print "Opened $json_filename\n";
my @json = <$ifh>;
close($ifh);

my $data = $json_xs->decode(join('',@json));
my %totals;

my @nodes = map {
    my %node;

    foreach my $key (keys %{$_}) {
        my $value = $_->{$key};
        if ( $totals{uc($key)} ) {
            no warnings;
            $totals{uc($key)} += $value;
            $totals{'MIN_' . uc($key)} = $value         if $value && $value < $totals{'MIN_' . uc($key)};
            $totals{'MAX_' . uc($key)} = $value         if $value > $totals{'MAX_' . uc($key)};
        }
        else {
            $totals{uc($key)} = $value;
            $totals{'MIN_' . uc($key)} = $value;
            $totals{'MAX_' . uc($key)} = $value;
        }

        if ( $key eq 'efficiency' || $key eq 'perfect' ) {
            $value = sprintf("%0.2f%%", $value*100);
        }
        elsif ( $key eq 'version' ) {
            my @words = split(/ /, $value);
            $value = $words[0];
            if ( ! $value =~ /^15/ ) {
                $node{WARNINGS} = 'Should be Version 15';
            }
        }
        elsif ( $key eq 'city' ) {
            $value //= $_->{region};
        }
        elsif ( $key eq 'uptime' ) {
            $node{UPTIME_RAW} = $value;
            $value = _convert_days_to_years_months_days_hours($value);
        }
        elsif ( $key eq 'warnings' ) {
            if ( !$value && $node{WARNINGS} ) {
                $value = $node{WARNINGS};
            }
        }

        $node{uc($key)} = $value;
    }
    \%node;
} @{$data->{nodes}};
$totals{HASHRATE} = sprintf "%.3f TH/s", ($totals{HASHRATE_RAW}  / 1000 / 1000 / 1000 / 1000);
$totals{NODE_COUNT} = scalar(@nodes);

my $template_data = {
    AUDIT_START     => _epoch_to_date_string($data->{audit_start}),
    AUDIT_END       => _epoch_to_date_string($data->{audit_end}),
    NODES           => [ @nodes ],
    %totals,
};


my $htmltemp = HTML::Template->new(filename => $tmpl_filename, die_on_bad_params => 0);
$htmltemp->param($template_data);
my @html = $htmltemp->output;
open(my $fh, ">", $html_filename) or die "Cannot open output $html_filename";
print $fh join('', @html);
close($fh);

$htmltemp = HTML::Template->new(filename => $tmpl_filenam2, die_on_bad_params => 0);
$htmltemp->param($template_data);
@html = $htmltemp->output;
open($fh, ">", $html_filenam2) or die "Cannot open output $html_filenam2";
print $fh join('', @html);
close($fh);

sub _epoch_to_date_string
{
    my $epoch = shift;
    use DateTime;

    my $dt = DateTime->from_epoch(epoch => $epoch);

    return join(' ',
                $dt->day,
                $dt->month_name,
                $dt->year,
                (sprintf "%d:%02d:%02d", $dt->hour, $dt->minute, $dt->second),
                $dt->am_or_pm
                );
}

sub _convert_days_to_years_months_days_hours
{
    my $in_days = shift;
    my $in_secs = (($in_days * 24) * 60) * 60;
    my $result;

    if ( $in_secs > (365 * 24 * 60 * 60) ) {
        $result .= sprintf "%d yr ", $in_secs / (365 * 24 * 60 * 60);
        $in_secs = $in_secs % (365 * 24 * 60 * 60);
    }
    if ( $in_secs > (30 * 24 * 60 * 60) ) {
        $result .= sprintf "%d mth ", $in_secs / (30 * 24 * 60 * 60);
        $in_secs = $in_secs % (30 * 24 * 60 * 60);
    }
    if ( $in_secs > (7 * 24 * 60 * 60) ) {
        $result .= sprintf "%d wk ", $in_secs / (7 * 24 * 60 * 60);
        $in_secs = $in_secs % (7 * 24 * 60 * 60);
    }
    if ( $in_secs > (24 * 60 * 60) ) {
        $result .= sprintf "%d day ", $in_secs / (24 * 60 * 60);
        $in_secs = $in_secs % (24 * 60 * 60);
    }
    if ( $in_secs > (60 * 60) ) {
        $result .= sprintf "%d hr ", $in_secs / (60 * 60);
        $in_secs = $in_secs % (60 * 60);
    }
    if ( $in_secs > (60) ) {
        $result .= sprintf "%d min ", $in_secs / (60);
        $in_secs = $in_secs % (60);
    }

    return $result;
}

